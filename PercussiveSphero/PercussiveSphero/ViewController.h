//
//  ViewController.h
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 2014/01/20.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    CGPoint point;
    BOOL isScroll;
    BOOL isAnimation;
    
    UIImageView *mainView;
    UIImageView *leftView;
    UIImageView *rightView;
    
    NSString* mainViewImageName;
    NSString* leftViewImageName;
    NSString* rightViewImageName;
    
    NSMutableArray *percussionNameList;
}
@property (weak, nonatomic) IBOutlet UILabel *percussionName1;
@property (weak, nonatomic) IBOutlet UILabel *percussionName2;
@property (weak, nonatomic) IBOutlet UILabel *percussionName3;
@property (weak, nonatomic) IBOutlet UIImageView *close1;
@property (weak, nonatomic) IBOutlet UIImageView *close2;
@property (weak, nonatomic) IBOutlet UIImageView *close3;

@end
