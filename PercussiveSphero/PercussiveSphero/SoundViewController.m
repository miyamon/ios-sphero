//
//  SoundViewController.m
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 2014/01/21.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import "SoundViewController.h"

@interface SoundViewController ()

@end

@implementation SoundViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    isActive = YES;
    
    imageViewList = [NSMutableArray array];
    soundDic = [NSMutableDictionary dictionary];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    
    robotOnline = NO;
    
    // 楽器の音を予め、ディクショナリに設定しておく
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundURL  = CFBundleCopyResourceURL (mainBundle, CFBridgingRetain(self.percussionNameList[currentIndex]),CFSTR ("mp3"),NULL);
    for (int i = 0; i < [self.percussionNameList count]; i++) {
        [soundDic setObject:CFBridgingRelease(soundURL) forKey:self.percussionNameList[i]];
    }
    [self appDidBecomeActive:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)appDidBecomeActive:(NSNotification*)notification {
    /*When the application becomes active after entering the background we try to connect to the robot*/
    [self setupRobotConnection];
}

-(void)startStatus {
    
    // Spheroの初期状態設定
    [RKSetDataStreamingCommand sendCommandWithSampleRateDivisor:0
                                                   packetFrames:0
                                                     sensorMask:RKDataStreamingMaskOff
                                                    packetCount:0];
   
    [[RKDeviceMessenger sharedMessenger] removeDataStreamingObserver:self];
    [RKStabilizationCommand sendCommandWithState:RKStabilizationStateOn];
    [RKBackLEDOutputCommand sendCommandWithBrightness:0.0f];
    [control closeConnection];
    robotOnline = NO;
}

-(void)appWillResignActive:(NSNotification*)notification {
    NSLog(@"appWillResignActive");
    /*When the application is entering the background we need to close the connection to the robot*/
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RKDeviceConnectionOnlineNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RKDeviceConnectionOfflineNotification object:nil];
    [self startStatus];
    for (int i = 0; i < [imageViewList count]; i++) {
     [imageViewList[i] removeFromSuperview];
    }
}



-(void)setupRobotConnection
{
    NSLog(@"setupRobotConnection");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRobotOnline) name:RKDeviceConnectionOnlineNotification object:nil];
    if ([[RKRobotProvider sharedRobotProvider] isRobotUnderControl]) {

        control = [[RKRobotProvider sharedRobotProvider] robotControl];
        currentIndex = 0;
        NSArray *robots = [[RKRobotProvider sharedRobotProvider] robots];
        
        control = [control initWithRobot:robots[currentIndex]];
        [control openConnection];

        // 楽器を表示する位置を設定
        int count = (int)[self.percussionNameList count];
        for (int i = 0; i < count; i++) {
            NSString *percussionName = self.percussionNameList[i];
            UIImage *img = [UIImage imageNamed:percussionName];
            
            UIImageView *iv = [[UIImageView alloc] initWithImage:img];
            iv.frame = CGRectMake(0, 0, 200, 200);
    
            CGFloat x = self.view.center.x;
            CGFloat y = self.view.center.y;
            
            if (i == 1) {
                y = y - 180;
            } else if (i == 2) {
                 y = y + 180;
            }
            iv.center = CGPointMake(x, y);
            [imageViewList addObject:iv];
            [self.view addSubview:iv];
        }
    } else {
        NSLog(@"else");
    }
}

-(void)leftMotionAnimation {
    NSLog(@"leftMotionAnimation");
    UIImageView *iv = imageViewList[currentIndex];
    if(!isExec) {
        [RKRGBLEDOutputCommand sendCommandWithRed:0.5 green:0.0 blue:0.0];
        iv.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        return;
    }
    // LED黄点灯
    [RKRGBLEDOutputCommand sendCommandWithRed:0.9 green:0.9 blue:0.0];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDuration:0.125f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(rightMotionAnimation)];
    iv.transform = CGAffineTransformMakeScale(-0.5f, 1.0f);
    [UIView commitAnimations];
    
}
-(void)rightMotionAnimation {
    NSLog(@"rightMotionAnimation");
    UIImageView *iv = imageViewList[currentIndex];
    if(!isExec) {
        iv.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        [RKRGBLEDOutputCommand sendCommandWithRed:0.5 green:0.0 blue:0.0];
        return;
    }
    // LED緑点灯
    [RKRGBLEDOutputCommand sendCommandWithRed:0.0 green:0.9 blue:0.0];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDuration:0.125f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(leftMotionAnimation)];
    iv.transform = CGAffineTransformMakeScale(0.5f, 1.0f);
    [UIView commitAnimations];
    
}

- (void)handleRobotOnline {
    NSLog(@"handleRobotOnline");
    /*The robot is now online, we can begin sending commands*/
    if(!robotOnline) {
        [RKSetDataStreamingCommand sendCommandStopStreaming];
        [RKStabilizationCommand sendCommandWithState:RKStabilizationStateOff];
        [RKBackLEDOutputCommand sendCommandWithBrightness:1.0f];
        
        [self sendSetDataStreamingCommand];
        
        [RKRGBLEDOutputCommand sendCommandWithRed:0.5 green:0.0 blue:0.0];
        [[RKDeviceMessenger sharedMessenger] addDataStreamingObserver:self selector:@selector(handleAsyncData:)];
    }
    robotOnline = YES;
}


-(void)sendSetDataStreamingCommand {
    
    // センサー部分はサンプルを流用
    RKDataStreamingMask mask =  RKDataStreamingMaskAccelerometerFilteredAll |
    RKDataStreamingMaskIMUAnglesFilteredAll   |
    RKDataStreamingMaskQuaternionAll;
    
    uint16_t divisor = 40;
    uint16_t packetFrames = 1;
    uint8_t count = 0;
    
    // Send command to Sphero
    [RKSetDataStreamingCommand sendCommandWithSampleRateDivisor:divisor
                                                   packetFrames:packetFrames
                                                     sensorMask:mask
                                                    packetCount:count];
    
    NSArray *robots = [[RKRobotProvider sharedRobotProvider] robots];
    // Spheroのペアリング数が複数の場合タイマーにより接続を切り替える
    if ([robots count] > 1) {
        [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(disconnect) userInfo:nil repeats:NO];
    }
}

- (void)handleAsyncData:(RKDeviceAsyncData *)asyncData
{
    // サンプル流用
    if ([asyncData isKindOfClass:[RKDeviceSensorsAsyncData class]]) {
        
        // Received sensor data, so display it to the user.
        RKDeviceSensorsAsyncData *sensorsAsyncData = (RKDeviceSensorsAsyncData *)asyncData;
        RKDeviceSensorsData *sensorsData = [sensorsAsyncData.dataFrames lastObject];
        RKAccelerometerData *accelerometerData = sensorsData.accelerometerData;
        
        // 振った時の加速度を２とする。
        int divValue = 2;
        if (divValue < accelerometerData.acceleration.x || divValue < accelerometerData.acceleration.y || divValue < accelerometerData.acceleration.z) {
            if (!isExec) {
                // 楽器の音を鳴らす
                SystemSoundID soundID;
                AudioServicesCreateSystemSoundID (CFBridgingRetain([soundDic objectForKey:self.percussionNameList[currentIndex]]), &soundID);
                AudioServicesPlaySystemSound (soundID);
                
                isExec = YES;
                [self leftMotionAnimation];
                [NSTimer scheduledTimerWithTimeInterval:0.4f target:self selector:@selector(stopExec:) userInfo:nil repeats:NO];
            }
        }
    }
}

-(void)stopExec:(NSTimer*)timer{
    isExec = NO;
}

-(void)disconnect {
    NSLog(@"disconnect");
    if (!isActive) {
        return;
    }
   
    NSArray *robots = [[RKRobotProvider sharedRobotProvider] robots];
    
    // 現在接続しているSpheroのインデックスがペアリングしているSpheroの数と同じ場合
    if (currentIndex == ([robots count] - 1)) {
        currentIndex = 0;
    } else {
        currentIndex++;
    }
    [self startStatus];
    control = [control initWithRobot:robots[currentIndex]];
    [control openConnection];
}

-(void)viewDidDisappear:(BOOL)animated {
    NSLog(@"viewDidDisappear");
    isActive = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [self appWillResignActive:nil];
}

@end
