//
//  main.m
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 2014/01/21.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
