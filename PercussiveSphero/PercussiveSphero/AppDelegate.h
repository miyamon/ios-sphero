//
//  AppDelegate.h
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 2014/01/21.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
