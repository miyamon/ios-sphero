//
//  SoundViewController.h
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 2014/01/21.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import <RobotKit/RobotKit.h>

@interface SoundViewController : UIViewController {
    BOOL robotOnline;
    BOOL isExec;
    BOOL isActive;
    
    NSMutableArray *imageViewList;
    NSMutableDictionary *soundDic;
    
    RKRobotControl *control;
    
    int currentIndex;
}
@property (nonatomic,nonatomic) NSMutableArray *percussionNameList;

-(void)setupRobotConnection;
-(void)handleRobotOnline;

@end
