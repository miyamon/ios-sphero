//
//  ViewController.m
//  PercussiveSphero
//
//  Created by 宮本 雅志 on 3014/01/20.
//  Copyright (c) 2014年 宮本 雅志. All rights reserved.
//

#import "ViewController.h"
#import "SoundViewController.h"
#import "RobotKit/RobotKit.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mainViewImageName = @"カスタネット.png";
    leftViewImageName = @"トライアングル.png";
    rightViewImageName = @"タンバリン.png";
    
    self.percussionName1.text = @"";
    self.percussionName2.text = @"";
    self.percussionName3.text = @"";
    
    percussionNameList = [NSMutableArray array];

    UIScrollView *sv = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    UIView *uv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, 100)];
    [sv addSubview:uv];
    sv.contentSize = uv.bounds.size;
    sv.showsHorizontalScrollIndicator = NO;// 横スクロールバー非表示
    sv.showsVerticalScrollIndicator = NO; // 縦スクロールバー非表示
    sv.scrollsToTop = NO; // ステータスバーのタップによるトップ移動禁止
    sv.delegate = self;
    [self.view addSubview:sv];
    [self.view sendSubviewToBack:sv];
    
    mainView = [self createImageView:mainViewImageName];
    [self.view addSubview:mainView];
    mainView.userInteractionEnabled = YES;
    mainView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(0, 10), 1.0f, 1.0f);
    
    leftView = [self createImageView:leftViewImageName];
    [self.view addSubview:leftView];
    leftView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-100, -50), 0.3f, 0.3f);
    
    rightView = [self createImageView:rightViewImageName];
    [self.view addSubview:rightView];
    rightView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(100, -50), 0.3f, 0.3f);
    
    point = CGPointMake(0, 0);
}

-(UIImageView *)createImageView: (NSString *)imageName {
    UIImage *img = [UIImage imageNamed:imageName];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    imgView.frame = CGRectMake(0, 0, 100, 100);
    imgView.center = self.view.center;
    imgView.tag = 11;
    return imgView;
}

-(void)scrollViewDidScroll: (UIScrollView *)scrollView {
    // スクロール中は常時実行
    if (point.x != 0 && isScroll) {
        if (point.x < scrollView.bounds.origin.x) {
            isAnimation = YES;
            isScroll = NO;
            [self rightScroll:mainView :leftView :rightView ];
        } else if (point.x > scrollView.bounds.origin.x) {
            isAnimation = YES;
            isScroll = NO;
            [self leftScroll:mainView :leftView :rightView ];
        }
    }
    point = scrollView.bounds.origin;
}

-(void)scrollViewWillBeginDragging: (UIScrollView *)scrollView {
    // スクロール開始時に呼び出される
    if (!isAnimation){
        isScroll = YES;
    }
}

-(void)leftScroll:(UIImageView *) mView :(UIImageView *) lView :(UIImageView *) rView{
    // 左回りアニメーション
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDuration:0.4f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endAnimation)];
    lView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(0, 10), 1.0f, 1.0f);
    rView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-100, -50), 0.3f, 0.3f);
    mView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(100, -50), 0.3f, 0.3f);
    [UIView commitAnimations];
    mainView = lView;
    leftView = rView;
    rightView = mView;
    
    NSString* temp = mainViewImageName;
    mainViewImageName = leftViewImageName;
    leftViewImageName = rightViewImageName;
    rightViewImageName = temp;
    
    mainView.userInteractionEnabled = YES;
    leftView.userInteractionEnabled = NO;
    rightView.userInteractionEnabled = NO;
}

-(void)rightScroll:(UIImageView *) mView :(UIImageView *) lView :(UIImageView *) rView{
    // 右回りアニメーション
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDuration:0.4f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endAnimation)];
    rView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(0, 10), 1.0f, 1.0f);
    mView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-100, -50), 0.3f, 0.3f);
    lView.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(100, -50), 0.3f, 0.3f);
    [UIView commitAnimations];
    mainView = rView;
    leftView = mView;
    rightView = lView;
    
    NSString* temp = mainViewImageName;
    mainViewImageName = rightViewImageName;
    rightViewImageName = leftViewImageName;
    leftViewImageName = temp;
    
    mainView.userInteractionEnabled = YES;
    leftView.userInteractionEnabled = NO;
    rightView.userInteractionEnabled = NO;
}

- (void) endAnimation {
    // アニメーション終了時に呼び出される
    isAnimation = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesBegan");
    UITouch *touch = [[event allTouches] anyObject];
    if ( touch.view.tag == self.close1.tag ) {
        self.percussionName1.text = @"";
        self.close1.hidden = YES;
        [percussionNameList removeObjectAtIndex:0];
    } else if ( touch.view.tag == self.close2.tag ) {
        self.percussionName2.text = @"";
        self.close2.hidden = YES;
        [percussionNameList removeObjectAtIndex:1];
    } else if ( touch.view.tag == self.close3.tag ) {
        self.percussionName3.text = @"";
        self.close3.hidden = YES;
        [percussionNameList removeObjectAtIndex:2];
    } else if ( touch.view.tag == mainView.tag ) {
        NSString *percussionName = [mainViewImageName stringByReplacingOccurrencesOfString:@".png" withString:@""];
        NSArray *robots = [[RKRobotProvider sharedRobotProvider] robots];
        int count = (int)[robots count];
        if (count == 1) {
            if ([percussionNameList count] == 1) {
                [percussionNameList removeObjectAtIndex:0];
            }
            self.percussionName1.text = [NSString stringWithFormat:@"楽器：%@", percussionName];
            self.close1.hidden = NO;
            [percussionNameList addObject:percussionName];
        } else if (count == 2) {
            if ([self.percussionName1.text isEqualToString:@""]) {
                self.percussionName1.text = [NSString stringWithFormat:@"楽器1：%@", percussionName];;
                self.close1.hidden = NO;
                [percussionNameList insertObject:percussionName atIndex:0];
            } else {
                self.percussionName2.text = [NSString stringWithFormat:@"楽器2：%@", percussionName];;
                self.close2.hidden = NO;
                if ([percussionNameList count] == 2) {
                    [percussionNameList removeObjectAtIndex:1];
                }
                [percussionNameList addObject:percussionName];
            }
        } else if (count == 3) {
            if ([self.percussionName1.text isEqualToString:@""]) {
                self.percussionName1.text = [NSString stringWithFormat:@"楽器1：%@", percussionName];;
                self.close1.hidden = NO;
                [percussionNameList insertObject:percussionName atIndex:0];
            } else if ([self.percussionName2.text isEqualToString:@""]) {
                self.percussionName2.text = [NSString stringWithFormat:@"楽器2：%@", percussionName];;
                self.close2.hidden = NO;
                [percussionNameList insertObject:percussionName atIndex:1];
            } else {
                self.percussionName3.text = [NSString stringWithFormat:@"楽器3：%@", percussionName];;
                self.close3.hidden = NO;
                if ([percussionNameList count] == 3) {
                    [percussionNameList removeObjectAtIndex:2];
                }
                [percussionNameList addObject:percussionName];
            }
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SoundViewController *dvc = segue.destinationViewController;
    dvc.percussionNameList = percussionNameList;
}
@end
